#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

//COmpilar :  gcc -pthread -o main teste.c

#define Buffer_Limit 10

int Buffer_Index_Value = 0;
int Buffer_Queue;

pthread_mutex_t mutex_variable = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t Buffer_Queue_Not_Full = PTHREAD_COND_INITIALIZER;
pthread_cond_t Buffer_Queue_Not_Empty = PTHREAD_COND_INITIALIZER;

void printInLogFile(char mensagem[100]){
  FILE *logFile = fopen("logFile.txt","a+");
  fprintf(logFile, "%s\n",mensagem);


  fclose(logFile);
}

void *Consumer(){
  char consumerOneMessage[50];
     while(1)
     {
        pthread_mutex_lock(&mutex_variable);
        if(Buffer_Index_Value == -1){
          pthread_cond_wait(&Buffer_Queue_Not_Empty, &mutex_variable);
        }
        if(Buffer_Queue != 0){
          snprintf (consumerOneMessage,50, "[consumo a]: Numero lido: ­%d",Buffer_Queue);
          printf("[consumo a]: Numero lido: ­%d\n",Buffer_Queue);
          printInLogFile(consumerOneMessage);
          Buffer_Queue = 0;
        }
        pthread_mutex_unlock(&mutex_variable);
        pthread_cond_signal(&Buffer_Queue_Not_Full);
        usleep(150);
        //sleep(15);
     }
}

void *Consumer2(){
    char consumerTwoMessage[50];
    while(1){
        pthread_mutex_lock(&mutex_variable);
        if(Buffer_Index_Value == -1){
          pthread_cond_wait(&Buffer_Queue_Not_Empty, &mutex_variable);
        }
        if(Buffer_Queue != 0){
          snprintf (consumerTwoMessage,50, "[consumo b]: Numero lido: ­%d",Buffer_Queue);
          printf("[consumo b]: Numero lido: ­%d\n",Buffer_Queue);
          printInLogFile(consumerTwoMessage);
          Buffer_Queue = 0;
        }
        pthread_mutex_unlock(&mutex_variable);
        pthread_cond_signal(&Buffer_Queue_Not_Full);
        usleep(150);
        //sleep(15);
    }
}

void *Producer(){
  char producerMessage[50];
    while(1){
        pthread_mutex_lock(&mutex_variable);

        if(Buffer_Index_Value == Buffer_Limit){
          pthread_cond_wait(&Buffer_Queue_Not_Full, &mutex_variable);
        }
        
        Buffer_Queue = (char)rand();
        snprintf (producerMessage,50, "[producao]: Numero gerado: ­%d",Buffer_Queue);
        printf("[producao]: Numero gerado: ­%d\n",Buffer_Queue);
        printInLogFile(producerMessage);
        pthread_mutex_unlock(&mutex_variable);
        pthread_cond_signal(&Buffer_Queue_Not_Empty);
        usleep(100);
         //sleep(10);
     }
}

void printOutput(){
  printf("\n[aviso]: Termino solicitado. Aguardando threads...\n");
  printInLogFile("[aviso]: Termino solicitado. Aguardando threads...");

  printf("[aviso]: Maior numero gerado: \n");
  printInLogFile("[aviso]: Maior numero gerado: ");

  printf("[aviso]: Menor numero gerado: \n");
  printInLogFile("[aviso]: Menor numero gerado: ");

  printf("[aviso]: Maior ocupacao de buffer: \n");
  printInLogFile("[aviso]: Maior ocupacao de buffer: ");

  printf("[aviso]: Aplicacao encerrada.\n");
  printInLogFile("[aviso]: Aplicacao encerrada.");

  exit(0);
}

int main()
{
  remove("logFile.txt");
   pthread_t producer_thread_id, consumer_thread_id,consumer2_thread_id;
   //Buffer_Queue = (int *) malloc(sizeof(int) * Buffer_Limit);

   pthread_create(&producer_thread_id, NULL, Producer, NULL);
   pthread_create(&consumer_thread_id, NULL, Consumer, NULL);
   pthread_create(&consumer2_thread_id, NULL, Consumer2, NULL);

   signal(2,printOutput);
   pthread_join(producer_thread_id, NULL);
   pthread_join(consumer_thread_id, NULL);
   pthread_join(consumer2_thread_id, NULL);



 return 0;
}

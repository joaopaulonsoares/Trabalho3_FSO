#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <signal.h>

#define BUFFER_SIZE 50

// MUTEX
pthread_mutex_t mutex;

// SEMAPHORES
sem_t filled, empty;

// BUFFER
typedef int buffer_item;
buffer_item buffer[BUFFER_SIZE];
int counter=0;
int biggestBufferCounter = -1;

// THREADS
pthread_t threadControl;
pthread_t threadProducer;
pthread_t threadConsumerOne;
pthread_t threadConsumerTwo;

// NUMBERS
int biggerNumber=0;
int smallerNumber=100000000;

//FUNCTIONS
void *producer(); /* the producer thread */
void *consumer(); /* the consumer thread */
void *consumer2();/* the consumer thread */
void *controlThreadFunction();

/* Function to print message in log file */
void printInLogFile(char mensagem[100]){
  FILE *logFile = fopen("logFile.txt","a+");
  fprintf(logFile, "%s\n",mensagem);
  fclose(logFile);
}

/* Add an item to the buffer */
int insert_item(buffer_item item) {
   /* When the buffer is not full add the item
      and increment the counter*/
   if(counter < BUFFER_SIZE) {
      buffer[counter] = item;
      counter++;

      if(counter>biggestBufferCounter){
        biggestBufferCounter = counter;
      }
      return 0;
   }
   else { /* Error the buffer is full */
      return -1;
   }
}

/* Remove an item from the buffer */
int remove_item(buffer_item *item) {
   /* When the buffer is not empty remove the item
      and decrement the counter */
   if(counter > 0) {
      *item = buffer[(counter-1)];
      counter--;
      return 0;
   }
   else { /* Error buffer empty */
      return -1;
   }
}

/* Set the bigger and the smaller number */
void confirmNumber(int number){

  if(number>biggerNumber){
    biggerNumber = number;
  }
  if(number<smallerNumber){
    smallerNumber = number;
  }

}

/* Producer Thread */
void *producerThreadFunction() {
   buffer_item item;
   char producerMessage[50];

   while(1) {
      /* generate a random number */
      item = rand();

      /* acquire the empty lock */
      sem_wait(&empty);
      /* acquire the mutex lock */
      pthread_mutex_lock(&mutex);

      if(insert_item(item)) {
         //fprintf(stderr, " Producer report error condition\n");
         printf("Producer report error condition\n");
      }
      else {
        printf("[producao]: Numero gerado: ­%d\n", item);
        snprintf (producerMessage,50, "[producao]: Numero gerado: ­%d",item);
        printInLogFile(producerMessage);
      }
      /* release the mutex lock */
      pthread_mutex_unlock(&mutex);
      /* signal filled */
      sem_post(&filled);
      usleep(100000);
   }
}

/* Consumer Threads */
void *consumerOneThreadFunction() {
   buffer_item item;
  char consumerOneMessage[50];

    while(1) {
      /* aquire the filled lock */
      sem_wait(&filled);
      /* aquire the mutex lock */
      pthread_mutex_lock(&mutex);

      if(remove_item(&item)) {
         //fprintf(stderr, "Consumer report error condition\n");
      }
      else {
        printf("[consumo a]: Numero lido: ­%d\n", item);
        snprintf (consumerOneMessage,50, "[consumo a]: Numero lido: ­%d",item);
        printInLogFile(consumerOneMessage);
        confirmNumber(item);
      }
      /* release the mutex lock */
      pthread_mutex_unlock(&mutex);
      /* signal empty */
      sem_post(&empty);
      usleep(150000);
   }
}

void *consumerTwoThreadFunction() {
   buffer_item item;
   char consumerTwoMessage[50];

   while(1) {
      /* aquire the filled lock */
      sem_wait(&filled);
      /* aquire the mutex lock */
      pthread_mutex_lock(&mutex);
      if(remove_item(&item)) {
        // fprintf(stderr, "Consumer report error condition\n");
      }
      else {
         printf("[consumo b]: Numero lido: ­%d\n", item);
         snprintf (consumerTwoMessage,50, "[consumo b]: Numero lido: ­%d",item);
         printInLogFile(consumerTwoMessage);
         confirmNumber(item);
      }
      /* release the mutex lock */
      pthread_mutex_unlock(&mutex);
      /* signal empty */
      sem_post(&empty);
      usleep(150000);
   }
}

void printOutput(){
  char printOutputMessage[50];

  printf("\n[aviso]: Termino solicitado. Aguardando threads...\n");
  printInLogFile("[aviso]: Termino solicitado. Aguardando threads...");

  printf("[aviso]: Maior numero gerado: %d\n",biggerNumber);
  snprintf (printOutputMessage,50, "[aviso]: Maior numero gerado: ­%d",biggerNumber);
  printInLogFile(printOutputMessage);

  printf("[aviso]: Menor numero gerado: %d\n",smallerNumber);
  snprintf (printOutputMessage,50, "[aviso]: Menor numero gerado: %d",smallerNumber);
  printInLogFile(printOutputMessage);

  printf("[aviso]: Maior ocupacao de buffer: %d \n",biggestBufferCounter);
  snprintf (printOutputMessage,50, "[aviso]: Maior ocupacao de buffer: %d",biggestBufferCounter);
  printInLogFile(printOutputMessage);

  printf("[aviso]: Aplicacao encerrada.\n");
  printInLogFile("[aviso]: Aplicacao encerrada.");

  exit(0);
}

void *controlThreadFunction() {
  while(1){
    signal(2,printOutput);
  }
}

int main(int argc, char *argv[]) {
      remove("logFile.txt");

   /* Initialize */
      /* Create the mutex lock */
      pthread_mutex_init(&mutex, NULL);
      /* Create the filled semaphore and initialize to 0 */
      sem_init(&filled, 0, 0);
      /* Create the empty semaphore and initialize to BUFFER_SIZE */
      sem_init(&empty, 0, BUFFER_SIZE);

  //THREADS
   /* Create the producer thread */
      pthread_create(&threadProducer,NULL,producerThreadFunction,NULL);
   /* Create the consumers threads */
      pthread_create(&threadConsumerOne,NULL,consumerOneThreadFunction,NULL);
      pthread_create(&threadConsumerTwo,NULL,consumerTwoThreadFunction,NULL);
    /* Create the control thread */
      pthread_create(&threadControl,NULL,controlThreadFunction,NULL);

    while (1) {
      /* code */
    }

  /* Exit the program */
  return  0;
}

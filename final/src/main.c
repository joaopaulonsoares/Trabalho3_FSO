#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#define BUFFER_SIZE 50

// Control end variable
int finishControl=0;

//FILE NAME
char fileName[50];

// MUTEX
pthread_mutex_t mutex;

// SEMAPHORES
sem_t filled, empty;

// BUFFER
typedef int buffer_item;
buffer_item buffer[BUFFER_SIZE];
int counter=0;
int biggestBufferCounter = -1;

// THREADS
pthread_t threadProducer;
pthread_t threadConsumerOne;
pthread_t threadConsumerTwo;

// NUMBERS
int biggerNumberConsumerOne=0;
int smallerNumberConsumerOne=100000000;
int biggerNumberConsumerTwo=0;
int smallerNumberConsumerTwo=100000000;
int biggerNumber;
int smallerNumber;


/* Function to print message in log file */
void printInLogFile(char mensagem[100]){
  FILE *logFile = fopen(fileName,"a+");
  fprintf(logFile, "%s\n",mensagem);
  fclose(logFile);
}

/* Add an item to the buffer */
void insert_item(buffer_item item) {
   /* When the buffer is not full add the item
      and increment the counter*/
   if(counter < BUFFER_SIZE) {
      buffer[counter] = item;
      counter++;

      if(counter>biggestBufferCounter){
        biggestBufferCounter = counter;
      }
   }
}

/* Remove an item from the buffer */
void remove_item(buffer_item *item) {
   /* When the buffer is not empty remove the item
      and decrement the counter */
   if(counter > 0) {
      *item = buffer[(counter-1)];
      counter--;
   }
}

/* Producer Thread */
void *producerThreadFunction() {
   buffer_item item;
   char producerMessage[50];

   while(1) {
      if(finishControl==1){
          break;
      }

      /* generate a random number */
      item = rand();

      /* acquire the empty lock */
      sem_wait(&empty);
      /* acquire the mutex lock */
      pthread_mutex_lock(&mutex);
      insert_item(item);
      printf("[producao]: Numero gerado: ­%d\n", item);
      snprintf (producerMessage,50, "[producao]: Numero gerado: ­%d",item);
      printInLogFile(producerMessage);
      /* release the mutex lock */
      pthread_mutex_unlock(&mutex);
      /* signal filled */
      sem_post(&filled);
      usleep(100000);
   }
   pthread_exit(NULL);
}

/* Consumer Threads */

void *consumerOneThreadFunction() {
  buffer_item item;
  char consumerOneMessage[50];

    while(1) {
      if(finishControl==1){
          break;
      }

      /* aquire the filled lock */
      sem_wait(&filled);
      /* aquire the mutex lock */
      pthread_mutex_lock(&mutex);
      remove_item(&item);

      printf("[consumo a]: Numero lido: ­%d\n", item);
      snprintf (consumerOneMessage,50, "[consumo a]: Numero lido: ­%d",item);
      printInLogFile(consumerOneMessage);

        if(item>biggerNumberConsumerOne){
          biggerNumberConsumerOne = item;
        }
        if(item<smallerNumberConsumerOne){
          smallerNumberConsumerOne = item;
        }


      /* release the mutex lock */
      pthread_mutex_unlock(&mutex);
      /* signal empty */
      sem_post(&empty);
      usleep(150000);
   }
   pthread_exit(NULL);
}

void *consumerTwoThreadFunction() {

   buffer_item item;
   char consumerTwoMessage[50];

   while(1) {
      if(finishControl==1){
          break;
      }

      /* aquire the filled lock */
      sem_wait(&filled);
      /* aquire the mutex lock */
      pthread_mutex_lock(&mutex);
      remove_item(&item);
      printf("[consumo b]: Numero lido: ­%d\n", item);
      snprintf (consumerTwoMessage,50, "[consumo b]: Numero lido: ­%d",item);
      printInLogFile(consumerTwoMessage);

      if(item>biggerNumberConsumerTwo){
        biggerNumberConsumerTwo = item;
      }
      if(item<smallerNumberConsumerTwo){
        smallerNumberConsumerTwo = item;
      }

      /* release the mutex lock */
      pthread_mutex_unlock(&mutex);
      /* signal empty */
      sem_post(&empty);
      usleep(150000);
   }
   pthread_exit(NULL);
}

void printOutput(){
  char printOutputMessage[50];

  finishControl=1;

  pthread_join(threadProducer,NULL);
  pthread_join(threadConsumerOne,NULL);
  pthread_join(threadConsumerTwo,NULL);

  printf("\n[aviso]: Termino solicitado. Aguardando threads...\n");
  printInLogFile("[aviso]: Termino solicitado. Aguardando threads...");


  if(biggerNumberConsumerOne>biggerNumberConsumerTwo){
      biggerNumber = biggerNumberConsumerOne;
  }else{
      biggerNumber = biggerNumberConsumerTwo;
  }
  printf("[aviso]: Maior numero gerado: %d\n",biggerNumber);
  snprintf (printOutputMessage,50, "[aviso]: Maior numero gerado: ­%d",biggerNumber);
  printInLogFile(printOutputMessage);


  if(smallerNumberConsumerOne>smallerNumberConsumerTwo){
      smallerNumber = smallerNumberConsumerTwo;
  }else{
      smallerNumber = smallerNumberConsumerOne;
  }
  printf("[aviso]: Menor numero gerado: %d\n",smallerNumber);
  snprintf (printOutputMessage,50, "[aviso]: Menor numero gerado: %d",smallerNumber);
  printInLogFile(printOutputMessage);

  printf("[aviso]: Maior ocupacao de buffer: %d \n",biggestBufferCounter);
  snprintf (printOutputMessage,50, "[aviso]: Maior ocupacao de buffer: %d",biggestBufferCounter);
  printInLogFile(printOutputMessage);

  printf("[aviso]: Aplicacao encerrada.\n");
  printInLogFile("[aviso]: Aplicacao encerrada.");


}

int main(int argc, char *argv[]) {
      strcpy(fileName,argv[1]);
      remove(fileName);
      srand((int)time(NULL));

   /* Initialize */
      /* Create the smutex lock */
      pthread_mutex_init(&mutex, NULL);
      /* Create the filled semaphore and initialize to 0 */
      sem_init(&filled, 0, 0);
      /* Create the empty semaphore and initialize to BUFFER_SIZE */
      sem_init(&empty, 0, BUFFER_SIZE);

  //THREADS
   /* Create the producer thread */
      pthread_create(&threadProducer,NULL,producerThreadFunction,NULL);
   /* Create the consumers threads */
      pthread_create(&threadConsumerOne,NULL,consumerOneThreadFunction,NULL);
      pthread_create(&threadConsumerTwo,NULL,consumerTwoThreadFunction,NULL);

      while(finishControl==0){
        signal(2,printOutput);
      }

  /* Exit the program */
  return  0;
}
